import itertools
import os
import cv2
import mediapipe as mp
import csv
import numpy as np
import pickle
import pandas as pd
from numpy import argmax
from pandas import read_csv
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import accuracy_score
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.metrics import Precision, Recall
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter
from keras.callbacks import EarlyStopping, ModelCheckpoint


mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_face_mesh = mp.solutions.face_mesh
data_path = '/home/ktrusi/datasets/DAiSEE/DataSet/'
dataset_path = os.listdir(data_path)
dataFrame = pd.read_csv('/home/ktrusi/datasets/DAiSEE/Labels/AllLabels.csv')
CONTOURS_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_CONTOURS)))
FACE_OVAL_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_FACE_OVAL)))
IRISES_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_IRISES)))
LEFT_EYE_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_LEFT_EYE)))
LEFT_EYEBROW_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_LEFT_EYEBROW)))
LEFT_IRIS_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_LEFT_IRIS)))
LIPS_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_LIPS)))
RIGHT_EYE_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_RIGHT_EYE)))
RIGHT_EYEBROW_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_RIGHT_EYEBROW)))
RIGHT_IRIS_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_RIGHT_IRIS)))
TESSELATION_INDEXES = list(set(itertools.chain(*mp_face_mesh.FACEMESH_TESSELATION)))


def load_csv_file(path):
    """
    Load csv file and read it with pandas
    :param path: path to csv file with data
    :return: andas dataFrame with data form csv file
    """
    return pd.read_csv(path + '.csv')


def load_paths(dataset):
    paths_list = []
    for ttv in dataset:
        if ttv.endswith('.txt'):
            continue
        users = os.listdir(data_path + '/' + ttv + '/')
        for user in users:
            curr_user = os.listdir(data_path + '/' + ttv + '/' + user + '/')
            for extract in curr_user:
                clip = os.listdir(data_path + '/' + ttv + '/' + user + '/' + extract + '/')[0]
                print(data_path + '/' + ttv + '/' + user + '/' + extract + '/' + clip)
                paths_list.append(data_path + '/' + ttv + '/' + user + '/' + extract + '/' + clip)

    return paths_list


def create_csv_file_mp(filename, indexes=None):
    total = []
    if indexes == None:
        num_coords = 478
    else:
        for obj in indexes:
            total = list(set(total + obj))
        num_coords = len(total)
    landmarks = ['class']
    for val in range(1, num_coords + 1):
        landmarks += ['x{}'.format(val), 'y{}'.format(val), 'z{}'.format(val)]

    # print(landmarks)

    with open(filename + '.csv', mode='w', newline='') as f:
        csv_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(landmarks)

    return filename + '.csv'


def create_samples(paths, filename, df, indexes, balance=False):
    # For webcam input:
    drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
    # paths2 = paths[0:200]
    paths2 = []
    dff = df.filter(items=['ClipID', 'Engagement'])
    X = dff.iloc[:, :-1]
    y = dff.iloc[:, -1]
    print("Before undersampling: ", Counter(y))
    undersample = RandomUnderSampler(sampling_strategy='auto')
    X_under, y_under = undersample.fit_resample(X, y)
    print("After undersampling: ", Counter(y_under))
    for p in paths:
        name = p.split('/')[-1]
        if name in X_under.values:
            paths2.append(p)

    paths = paths2
    total = []
    for obj in indexes:
        total = list(set(total + obj))
    for i, path in enumerate(paths):
        cap = cv2.VideoCapture(path)
        counter = 0
        with mp_face_mesh.FaceMesh(
                max_num_faces=1,
                refine_landmarks=True,
                min_detection_confidence=0.5,
                min_tracking_confidence=0.5) as face_mesh:
            while cap.isOpened():
                success, image = cap.read()
                counter = counter + 1
                if not success:
                    print(str(i) + "/" + str(len(paths)))
                    # If loading a video, use 'break' instead of 'continue'.
                    break

                # To improve performance, optionally mark the image as not writeable to
                # pass by reference.
                image.flags.writeable = False
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                results = face_mesh.process(image)

                # Draw the face mesh annotations on the image.
                image.flags.writeable = True
                # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                if counter == 5:
                    counter = 0
                    try:
                        face = results.multi_face_landmarks[0].landmark
                        # face_row = list(np.array(
                        #     [[round(landmark.x, 3), round(landmark.y, 3), round(landmark.z, 3)] for landmark in
                        #      face]).flatten())
                        if indexes is None:
                            # face_row = list(np.array(
                            #     [[landmark.x, landmark.y, landmark.z] for landmark in
                            #      face]).flatten())
                            face_row = list(np.array(
                                [[round(landmark.x, 3), round(landmark.y, 3), round(landmark.z, 3)] for landmark in
                                 face]).flatten())
                        else:
                            face_row = list(np.array(
                                [[round(face[i].x, 3), round(face[i].y, 3), round(face[i].z, 3)] for i in
                                 total]).flatten())

                        name = path.split('/')[-1]
                        row = df.loc[df['ClipID'] == name]
                        class_name = row.iat[0, 2]

                        face_row.insert(0, class_name)

                        with open(filename + '.csv', mode='a', newline='') as f:
                            csv_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            csv_writer.writerow(face_row)
                    except:
                        print('error' + str(i))
                        continue

        cap.release()


def train_model(csv_file):


    X = csv_file.drop('class', axis=1)
    Y = csv_file['class']

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3, random_state=1234)

    pipelines = {
        # 'lr': make_pipeline(StandardScaler(), LogisticRegression(solver='lbfgs', max_iter=10000)),
        'rc': make_pipeline(StandardScaler(), RidgeClassifier()),
        'rf': make_pipeline(StandardScaler(), RandomForestClassifier()),
        'gb': make_pipeline(StandardScaler(), GradientBoostingClassifier()),
        'knn': make_pipeline(StandardScaler(), KNeighborsClassifier()),
        'svm': make_pipeline(StandardScaler(), SVC(gamma='auto'))
    }
    fit_models = {}
    for algo, pipeline in pipelines.items():
        model = pipeline.fit(X_train.values, Y_train)
        fit_models[algo] = model

    for algo, model in fit_models.items():
        yhat = model.predict(X_test)
        print(algo, accuracy_score(Y_test, yhat))

    return fit_models


def open_model_pkl(path):
    """
    Function opens pickle file with model data
    :param path: path to pickle file
    :return: classification model
    """

    with open(path + '.pkl', 'rb') as f:
        model = pickle.load(f)

    return model


def save_model_pkl(models, filename, mode='rf'):
    """
    Function serializes model and saves it to pickle file
    :param models: Dictionary with models
    :param filename: Name for the pickle file that will be created
    :param mode: key of models dictionary, pass 'all' to save all models. Defaults to 'rf'.
    :return: name of saved file
    """

    if mode == 'all':
        with open(filename + '.pkl', 'wb') as f:
            pickle.dump(models, f)
    else:
        with open(filename + '.pkl', 'wb') as f:
            pickle.dump(models[mode], f)

    return filename + '.pkl'


def predict(model=None, model_nn=None, indexes=None):
    drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
    cap = cv2.VideoCapture(0)
    total = []
    for obj in indexes:
        total = list(set(total + obj))
    with mp_face_mesh.FaceMesh(
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as face_mesh:
      while cap.isOpened():
        success, image = cap.read()
        if not success:
          print("Ignoring empty camera frame.")
          # If loading a video, use 'break' instead of 'continue'.
          continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_mesh.process(image)

        # Draw the face mesh annotations on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        face = results.multi_face_landmarks[0].landmark
        # face_row = list(np.array(
        #     [[round(landmark.x, 3), round(landmark.y, 3), round(landmark.z, 3)] for landmark in
        #      face]).flatten())
        if indexes is None:
            # face_row = list(np.array(
            #     [[landmark.x, landmark.y, landmark.z] for landmark in
            #      face]).flatten())
            face_row = list(np.array(
                [[round(landmark.x, 3), round(landmark.y, 3), round(landmark.z, 3)] for landmark in
                 face]).flatten())
        else:
            face_row = list(np.array(
                [[round(face[i].x, 3), round(face[i].y, 3), round(face[i].z, 3)] for i in
                 total]).flatten())

        # Predict
        X = pd.DataFrame([face_row])
        if model is not None:
            att_class = model.predict(X)[0]
            att_prob = model.predict_proba(X)[0]

        if model_nn is not None:
            att_class_nn = model_nn.predict(X)[0]
            max_index = np.argmax(att_class_nn)
            att_prob_nn = att_class_nn[max_index]
        # os.system('clear')
        # print(body_language_class, body_language_prob)

        image = cv2.flip(image, 1)
        cv2.rectangle(image, (0, 0), (280, 60), (245, 117, 16), -1)

        # Display Class
        cv2.putText(image, 'CLASS'
                    , (95, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
        if model is not None:
            cv2.putText(image, str(att_class)
                        , (90, 33), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)
            cv2.putText(image, str(round(att_prob[np.argmax(att_prob)], 2))
                        , (10, 33), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)

        if model_nn is not None:
            cv2.putText(image, str(max_index)
                        , (90, 53), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)
            cv2.putText(image, str(round(att_prob_nn, 2))
                        , (10, 53), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)


        # Display Probability
        cv2.putText(image, 'PROB'
                    , (15, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)



        cv2.putText(image, "Classificator"
                    , (120, 33), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(image, "NN"
                    , (120, 53), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1, cv2.LINE_AA)

        # Flip the image horizontally for a selfie-view display.
        cv2.imshow('MediaPipe Face Mesh', image)
        if cv2.waitKey(5) & 0xFF == 27:
          break
    cap.release()


def build_nn(path):
    df = read_csv(path)
    # X, y = df.values[:, :-1], df.values[:, -1]
    X, y = df.values[:, 1:], df.values[:, 0]
    # X = X.astype('float32')
    y = LabelEncoder().fit_transform(y)
    # y = pd.get_dummies(y)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
    n_features = X_train.shape[1]
    model = Sequential()
    model.add(Dense(160, activation='relu', kernel_initializer='he_normal', input_shape=(n_features,)))
    model.add(Dense(100, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(70, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(30, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(10, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(4, activation='softmax'))
    # compile the model
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    es = EarlyStopping(monitor='accuracy', mode='max', verbose=1, patience=100)
    mc = ModelCheckpoint('best_model.h5', monitor='accuracy', mode='max', verbose=1, save_best_only=True)
    model.fit(X_train, y_train, epochs=500, callbacks=[es, mc])

    model = load_model('best_model.h5')

    test = model.evaluate(X_test, y_test)
    print(test)

    # predict probabilities for test set
    yhat_probss = model.predict(X_test, verbose=0)
    # predict crisp classes for test set
    yhat_classes = np.argmax(yhat_probss, axis=1)
    yhat_probs = yhat_probss[:, 0]
    # yhat_classes = yhat_classes[:, 0]
    # accuracy: (tp + tn) / (p + n)
    accuracy = accuracy_score(y_test, yhat_classes)
    print('Accuracy: %f' % accuracy)
    # precision tp / (tp + fp)
    precision = precision_score(y_test, yhat_classes, average='weighted', zero_division=1)
    print('Precision: %f' % precision)
    # recall: tp / (tp + fn)
    recall = recall_score(y_test, yhat_classes, average='weighted')
    print('Recall: %f' % recall)
    # f1: 2 tp / (2 tp + fp + fn)
    f1 = f1_score(y_test, yhat_classes, average='weighted')
    print('F1 score: %f' % f1)
    stop = 0
    model.save('modelNN2')
    return model


# paths = load_paths(dataset_path)
# create_csv_file_mp('samples6', [CONTOURS_INDEXES, IRISES_INDEXES])
# landmarks = ['class', 'landmarks']
# create_samples(paths, 'samples6', dataFrame, [CONTOURS_INDEXES, IRISES_INDEXES], True)
# file = load_csv_file('samples6')
# models = train_model(file)
# save_model_pkl(models, 'model_s6', 'knn')
model = open_model_pkl('model_s6')
# modelNN = build_nn('samples6.csv')
# build_nn('testiris.csv')
# modelNN = load_model('modelNN1')
modelNN = load_model('best_model.h5')

predict(model=model, model_nn=modelNN, indexes=[CONTOURS_INDEXES, IRISES_INDEXES])

